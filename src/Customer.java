import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Customer {

    private final String name;

    private List<Order> orderList = new ArrayList<>();

    public Customer(String Name) {
        this.name = Name;
    }

    public void add(Order item) {
        orderList.add(item);
    }
    //jauna metode addOrder, kas pievienoja jaunus orderus pie Orderlist

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", orderList=" + orderList +
                '}';
    }

    public String Getinfo() {
        return name + orderList.size();
    }

    public static void main(String[] args) throws ParseException, IOException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        File file = new File("input.txt");
        Customer jauns = new Customer("Janis");
        List<Order> orderList;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            orderList = reader.lines().map(line -> parseToOrder(line, jauns, formatter))
                    .collect(Collectors.toList());
        }
        System.out.println(orderList);
        Date date = new Date();
        Stream<Order> orderStream = orderList.stream().filter((Order o) -> o.isAfter(date));
        List<Order> collect = orderStream.collect(Collectors.toList());
        System.out.println(collect);

    }

    public static Order parseToOrder(String line, Customer customer, SimpleDateFormat formatter) {
        String[] result = line.split("\t");
        return new Order(result[0], customer, getDate(formatter, result[1]));
    }

    private static Date getDate(SimpleDateFormat formatter, String line) {
        try {
            return formatter.parse(line);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}

//lauks string name
//Arraylist no orderiem
//japievieno orderus